#!/usr/bin/python3

import os 
import zipfile

def backupDir2Zip(folder, zipname):
    #Eine Ebene tiefer für relpath
    os.chdir(folder)
    os.chdir("..")
    folder = os.path.relpath(folder)
    zipFilename = zipname + ".zip"
   
    print("Erstelle Zip Datei: " + zipFilename)
    backupfile=zipfile.ZipFile(zipFilename,  "w",  zipfile.ZIP_DEFLATED)
    #backupfile=zipfile.ZipFile(zipFilename,  "w",  zipfile.ZIP_LZMA)
    #backupfile=zipfile.ZipFile(zipFilename,  "w",  zipfile.ZIP_BZIP2)
 
    
    #Ordner rekursiv durchgehe
    for ordner, unterordner,  dateien in os.walk(folder):
        backupfile.write(ordner)
        for datei in dateien:
            backupfile.write(os.path.join(ordner, datei))
    
    backupfile.close
    
if __name__ == '__main__':
    backupDir2Zip("/home/udatt/Files/Proggen/python-space/pyBakFTP/2019-07-06/",  "test") 
        
