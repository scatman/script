#!/usr/bin/python3

import ftplib
import os
import shutil
import datetime
from . import backupdir

# ToDo Ordner zippen
# ToDo print(lösche Ordner, Ascii fehler) UTF8 einstellen?
# Order oder Datei vorhanden Fehler abfangen
# beim erstellen--> zB datstring inkl sekunden
# Mehr Fehler abfragen
########################################
# if __name__ == "__main__":
#    ftphostname = 'your-ftp-host'
#    ftpusername = 'ftp-username'
#    ftppassword = 'ftp-passwd'
#    ftpstart_directory = '/' #DokuWiki = conf, data und lib
#    ftpbackup_dir = 'where-you-want-to-safe'
#    main( ftphostname,  ftpusername,  ftppassword,  ftpstart_directory,  ftpbackup_dir)
########################################    

def main(ftphostname, ftpusername, ftppassword, ftpstart_directory, ftpbackup_dir):
   
    def get_files_directories():
        dirlisting = []
        ftp_obj.retrlines('LIST',callback=dirlisting.append)
        files = []
        directories = []
        for l in dirlisting:
            lastspace = l.rindex(' ')
            file_name = l[lastspace+1:]
            if l[0] == 'd' and file_name != '.' and file_name != '..':
                directories.append(file_name)
            elif l[0] == '-':
                files.append(file_name)
        return files,directories

    def backup_directory(local_dir, remote_dir):
        os.chdir(local_dir) #in backupdir
        try:
            ftp_obj.cwd(remote_dir)
            print('In directory '+remote_dir)
            files, directories = get_files_directories()
            for f in files:
                print('Backing up '+f)
                try:
                    ftp_obj.retrbinary('RETR '+f, open(f, 'wb').write)
                except ftplib.error_perm:
                    print('Skipping '+f+' due to permissions')
                    continue

            for d in directories:
                newremote = remote_dir+d+'/'
                newlocal = local_dir+'/'+d
                os.mkdir(newlocal)
                print(newlocal)
                backup_directory(newlocal, newremote)
        except:
            print('Skipping '+remote_dir+' due to permissions!!!!!')

    os.chdir(ftpbackup_dir) # change where to save
    datestring = str(datetime.date.today()) # string date today

    os.mkdir(datestring) # mk dir with data and change to
    os.chdir(datestring)

    #ftp opject
    ftp_obj = ftplib.FTP(host=ftphostname, user=ftpusername, passwd=ftppassword)

    local_dir = os.getcwd()+'/' #absolute path
    remote_dir = ftpstart_directory# start dir on webserver
    # backup dirs for dokuwiki 
    wikifolder=['conf/', 'data/pages/', 'data/meta', 'data/media/', 'data/attic/']

    for wikisupfolder in wikifolder:
        os.chdir(local_dir)# ichange in backupdir
        os.makedirs(wikisupfolder) # Ordner mit Datum erstellen
        backup_directory(local_dir + wikisupfolder, remote_dir + '/' + wikisupfolder)
    ftp_obj.quit()

    #Datei zippen
    backupdir.backupDir2Zip(local_dir, datestring+"-dokuwiki")


    #Ordner löschen
    print("Zip erstellt, loesche Ordner")
    shutil.rmtree(local_dir)




