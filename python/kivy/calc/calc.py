#!/usr/bin/env python3

'''
reference: https://www.geeksforgeeks.org/how-to-make-calculator-using-kivy-python/
'''

import kivy     
from kivy.app import App
    
from kivy.uix.gridlayout import GridLayout
 
# for the size of window
from kivy.config import Config
Config.set('graphics', 'resizable', 1)

# keybinds
from kivy.core.window import Window 

#platform check
from kivy.utils import platform

# Creating Layout class
class CalcGridLayout(GridLayout):

    #https://www.reddit.com/r/kivy/comments/8cx09b/how_to_get_keydown_event_in_kivy/?utm_source=share&utm_medium=web2x&context=3
    def __init__(self, **kwargs):
        super(CalcGridLayout, self).__init__(**kwargs)

        # check android
        # https://stackoverflow.com/a/56658009
        if ( kivy.utils.platform != 'android' ):
            self._keyboard = Window.request_keyboard(self.press, self)
            self._keyboard.bind(on_key_down=self.press)
        
    def press(self, keyboard, keycode, text, modifiers):
        key = keycode[1]
        print(key)
        if key in '01234567890+-/*':
            # https://gist.github.com/JoshuaManuel/2185740e395062a08136aeec819c3e7f
            self.ids[key].trigger_action(0)  # https://kivy.org/doc/stable/guide/lang.html#accessing-widgets-defined-inside-kv-lang-in-your-python-code
        elif key == 'enter' or key == '=':
            self.ids['='].trigger_action(0)
        elif key == 'spacebar':
            self.ids['ac'].trigger_action(0)
        elif key == 'escape':
            # self.root_window.close()
            App.get_running_app().stop()

        return True
    
    def key_action(self, *args):
        print("got a key event: %s" % list(args))

    # Function called when equals is pressed
    def calculate(self, calculation):
        if calculation:
            try:
                # Solve formula and display it in entry
                # which is pointed at by display
                self.display.text = str(eval(calculation))
            except Exception:
                self.display.text = "Error"

    def key_action(self, *args):
        print("got a key event: %s" % list(args))

 # Creating App class
class CalcApp(App):
  
    def build(self):
        return CalcGridLayout()
  
# creating object and running it
calcApp = CalcApp()
calcApp.run()