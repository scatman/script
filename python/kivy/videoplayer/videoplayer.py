from kivy.app import App
from kivy.uix.video import Video
import time
 
 
class SimpleApp(App):
    def build(self):
 
        stream = Video(source="rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mp4", play=True)
        return stream
 
 
if __name__ == "__main__":
    SimpleApp().run()