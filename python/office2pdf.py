#!/usr/bin/env python3

import os
from inotify_simple import INotify, flags
import subprocess


officedir = "/home/user/office"  # Watch this dir
os.chdir(officedir)

# filetype list
exts = [".ppt", ".pptx", ".doc", "docx"]

# command if event in this dir
popenlist = ['soffice', '--headless', '--convert-to', 'pdf', 'filename']

# Inotify
inotify = INotify()
watchflags = flags.CLOSE_WRITE  # only watch for modify
# watch_flags = flags.CREATE | flags.DELETE | flags.MODIFY | flags.DELETE_SELF
wd = inotify.add_watch(officedir, watchflags)

while True:
    for event in inotify.read():  # start watching
        for flag in flags.from_mask(event.mask):
            # check filetype
            # print('    ' + str(flag))
            for ext in exts:
                if event.name.endswith(ext):
                    print("Convert: ", event.name, " ", flag)
                    popenlist[4] = event.name  # set filname
                    subprocess.Popen(popenlist)  # start convert2pdf
